import testinfra.utils.ansible_runner
import time
import xml.etree.cElementTree as ET

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')

started = False


def wait_for_application_startup(host):
    global started
    if not started:
        for i in range(10):
            cmd = host.run('curl -k -s -o /dev/null -L -w "%{http_code}" https://ics-ans-role-naming-convention-tool-default')
            if cmd.stdout == '200':
                started = True
                return
            time.sleep(5)
        raise RuntimeError('Timed out waiting for application to start.')


def test_naming_web(host):
    wait_for_application_startup(host)
    cmd = host.run('curl -k https://ics-ans-role-naming-convention-tool-default/')
    assert '<title>Naming Service</title>' in cmd.stdout


def test_naming_rest_api(host):
    wait_for_application_startup(host)
    cmd = host.run('curl -k https://ics-ans-role-naming-convention-tool-default/rest/deviceNames')
    assert ET.fromstring(cmd.stdout).tag == 'collection'
