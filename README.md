ics-ans-role-naming-convention-tool
===================

Ansible role to install naming-convention-tool.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
naming_network: naming-network
naming_container_name: naming
naming_hostname: "{{ ansible_fqdn }}"
naming_version: latest
naming_image: "registry.esss.lu.se/ics-software/naming-convention-tool:{{ naming_version }}"
naming_server_port: 8080
naming_logs_dir: /var/log/naming
naming_database_username: naming
naming_database_password: naming
naming_database_host: naming-database
naming_database_port: 5432
naming_database_name: discs_names
naming_database_url: "jdbc:postgresql://{{ naming_database_host }}:{{ naming_database_port }}/{{ naming_database_name }}"

naming_database_container: true
naming_database_container_name: "{{ naming_database_host }}"
naming_database_image: postgres:9
naming_database_dir: "/var/naming-convention-tool/database"
naming_database_env:
  POSTGRES_USER: "{{ naming_database_username }}"
  POSTGRES_PASSWORD: "{{ naming_database_password }}"
  POSTGRES_DB: "{{ naming_database_name }}"

traefik_network: traefik-network
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-traefik
    - role: ics-ans-role-naming-convention-tool
```

License
-------

BSD 2-clause
